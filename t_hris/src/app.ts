import express, { Request, Response } from 'express';
import cors from 'cors';
import employeeRoutes from './routes/employeeRoutes';

const app = express();

// Middleware
app.use(express.json());
app.use(cors());

// Routes
app.use('/employees', employeeRoutes);

// Error handling middleware
app.use((err: Error, req: Request, res: Response) => {
    console.error('Error:', err);
    res.status(500).json({ message: 'Internal server error' });
});


export default app;
