// src/controllers/authController.ts
import { Request, Response } from 'express';
import { Pool } from 'pg';

const pool = new Pool({
    user: 'db_user',
    host: 'localhost',
    database: 'db_name',
    password: 'db_password',
    port: 5432,
});

export const employeeList = async (req: Request, res: Response) => {

    try {
        res.status(200).json({
            code: 200,
            message: "List of Employee"
        });
    } catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};

export const employeeCreate = async (req: Request, res: Response) => {

    try {
        res.status(200).json({
            code: 200,
            message: "Create Employee",
            data: req.body,
        });
    } catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};

export const employeeUpdate = async (req: Request, res: Response) => {

    try {
        res.status(200).json({
            code: 200,
            message: "Update employee",
            data: req.body,
        });
    } catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};

export const employeeDelete = async (req: Request, res: Response) => {

    try {
        res.status(200).json({
            code: 200,
            message: "Delete employee",
            data: req.body,
        });
    } catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};