import { Router } from 'express';
import { employeeList, employeeCreate, employeeUpdate, employeeDelete } from '../controllers/employeeController';

const router = Router();

router.get('/', employeeList);
router.post('/create', employeeCreate);
router.put('/update', employeeUpdate);
router.delete('/delete', employeeDelete);

export default router;
