"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.employeeDelete = exports.employeeUpdate = exports.employeeCreate = exports.employeeList = void 0;
const pg_1 = require("pg");
const pool = new pg_1.Pool({
    user: 'db_user',
    host: 'localhost',
    database: 'db_name',
    password: 'db_password',
    port: 5432,
});
const employeeList = async (req, res) => {
    try {
        res.status(200).json({
            code: 200,
            message: "List of Employee"
        });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.employeeList = employeeList;
const employeeCreate = async (req, res) => {
    try {
        res.status(200).json({
            code: 200,
            message: "Create Employee",
            data: req.body,
        });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.employeeCreate = employeeCreate;
const employeeUpdate = async (req, res) => {
    try {
        res.status(200).json({
            code: 200,
            message: "Update employee",
            data: req.body,
        });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.employeeUpdate = employeeUpdate;
const employeeDelete = async (req, res) => {
    try {
        res.status(200).json({
            code: 200,
            message: "Delete employee",
            data: req.body,
        });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.employeeDelete = employeeDelete;
