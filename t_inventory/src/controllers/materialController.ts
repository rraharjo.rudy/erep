import { Request, Response } from 'express';

export const materialList = async (req: Request, res: Response) => {

    try {
        res.status(200).json({
            code: 200,
            message: "list of materials"
        });
    } catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};

export const materialCreate = async (req: Request, res: Response) => {

    try {
        res.status(200).json({
            code: 200,
            message: "Create Material",
            data: req.body,
        });
    } catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};

export const materialUpdate = async (req: Request, res: Response) => {

    try {
        res.status(200).json({
            code: 200,
            message: "Update Material",
            data: req.body,
        });
    } catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};

export const materialDelete = async (req: Request, res: Response) => {

    try {
        res.status(200).json({
            code: 200,
            message: "Delete Material",
            data: req.body,
        });
    } catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};