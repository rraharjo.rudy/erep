import { Router } from 'express';
import { materialList, materialCreate, materialUpdate, materialDelete } from '../controllers/materialController';

const router = Router();

router.get('/', materialList);
router.post('/create', materialCreate);
router.put('/update', materialUpdate);
router.delete('/delete', materialDelete);

export default router;
