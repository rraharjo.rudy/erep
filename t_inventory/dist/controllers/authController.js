"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = exports.login = void 0;
const pg_1 = require("pg");
const pool = new pg_1.Pool({
    user: 'db_user',
    host: 'localhost',
    database: 'db_name',
    password: 'db_password',
    port: 5432,
});
const login = async (req, res) => {
    const { username, password } = req.body;
    try {
        res.status(200).json({
            code: 200,
            message: "route login"
        });
        // res.status(200).json({ 'status': true, 'message': 'route login' });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.login = login;
const register = async (req, res) => {
    const { username, password } = req.body;
    try {
        res.status(200).json({ 'status': true, 'message': 'route register' });
    }
    catch (error) {
        res.status(500).json({ message: 'Internal server error' });
    }
};
exports.register = register;
