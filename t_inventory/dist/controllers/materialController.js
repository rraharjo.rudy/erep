"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.materialDelete = exports.materialUpdate = exports.materialCreate = exports.materialList = void 0;
const materialList = async (req, res) => {
    try {
        res.status(200).json({
            code: 200,
            message: "list of materials"
        });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.materialList = materialList;
const materialCreate = async (req, res) => {
    try {
        res.status(200).json({
            code: 200,
            message: "Create Material",
            data: req.body,
        });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.materialCreate = materialCreate;
const materialUpdate = async (req, res) => {
    try {
        res.status(200).json({
            code: 200,
            message: "Update Material",
            data: req.body,
        });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.materialUpdate = materialUpdate;
const materialDelete = async (req, res) => {
    try {
        res.status(200).json({
            code: 200,
            message: "Delete Material",
            data: req.body,
        });
    }
    catch (error) {
        res.status(500).json({
            code: 200,
            message: "Internal server error"
        });
    }
};
exports.materialDelete = materialDelete;
