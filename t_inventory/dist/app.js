"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
// import authRoutes from './routes/authRoutes';
const materialRoutes_1 = __importDefault(require("./routes/materialRoutes"));
const app = (0, express_1.default)();
// Middleware
app.use(express_1.default.json());
app.use((0, cors_1.default)());
// Routes
app.use('/materials', materialRoutes_1.default);
// Error handling middleware
app.use((err, req, res) => {
    console.error('Error:', err);
    res.status(500).json({ message: 'Internal server error' });
});
exports.default = app;
